import React, {useContext, useEffect} from 'react';
import Header from '@components/Header';
import AppContext from '../context/AppContext';
import UseGetSession from '../hooks/useGetSession';

const Layout = ({ children }) => {
	const {state, updateSessionState} = useContext(AppContext)
	const session = UseGetSession();
	useEffect(()=>{
		updateSessionState(session)
	}, [session])

	return (
		<div className="Layout">
			<Header />
			{children}
		</div>
	);
}

export default Layout;

import React, {useContext} from 'react';
import ProductItem from '@components/ProductItem';
import UseGetProducts from '@hooks/useGetProducts';
import '@styles/ProductList.scss';
import ProductDetail from '@containers/ProductDetail';
import AppContext from '../context/AppContext';

const ProductList = ({category}) => {
	const API = category ? `http://localhost:3000/products/${category}` : 'http://localhost:3000/products';
	const products = UseGetProducts(API, category);
	const { state } = useContext(AppContext);
	return (
		<section className="main-container">
			<div className="ProductList">
				{products.map(product => (
					<ProductItem product={product} key={product.id}/>
				))}
				
			</div>
			{state.openDetail && <ProductDetail />}
		</section>
	);
}

export default ProductList;

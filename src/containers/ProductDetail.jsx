import React, { useContext } from 'react';
import ProductInfo from '@components/ProductInfo';
import '@styles/ProductDetail.scss';
import image from '@icons/icon_close.png';
import AppContext from '../context/AppContext';


const ProductDetail = () => {
	const { toggleItemDetail } = useContext(AppContext);

	const handleToggleItemDetail = () => {
		toggleItemDetail(false);
	}

	return (
		<aside className="ProductDetail">
			<div className="ProductDetail-close" onClick={handleToggleItemDetail}>
				<img src={image} alt="close" />
			</div>
			<ProductInfo />
		</aside>
	);
}

export default ProductDetail;

import React, { useContext } from 'react';
import OrderItem from '@components/OrderItem';
import Menu from '@components/Menu';
import '@styles/Checkout.scss';
import AppContext from '../context/AppContext';
import axios from 'axios';
const API = 'http://localhost:3000/checkout'

const Checkout = () => {


	const { state } = useContext(AppContext);
	let newDate = new Date();
	console.log(state)
	

	const sumTotal = () => {
		const reducer = (accumulator, currentValue) => accumulator + (currentValue.price * currentValue.amount);
		const sum = state.cart.reduce(reducer, 0);
		return sum;
	}


	const updatecompra = async (event) => {
		event.preventDefault();
			const data = {
				idUsuario: state.session.infoSession.id,
				productos: state.cart,
				total: sumTotal()
			}
			const response = await axios.put(API, data);
			console.log(response);
			alert('tu compra ya esta hecho')
	}
	return (
		<div className="Checkout">
			<div className="Checkout-container">
				<h1 className="title">Verificar orden</h1>
				<div className="Checkout-content">
					<div className="order">
						<p>
							<span></span>
							<span>{state.cart.length} {state.cart.length == 1 ? 'producto' : 'productos'}</span>
						</p>
						<p>${sumTotal()}</p>
					</div>
				</div>
				{state.cart.map(product => (
					<OrderItem product={product} key={`orderItem-${product.id}`} />
				))}
				<button className='primary-button' onClick={updatecompra}>comprar</button>
			</div>
		</div>
	);

}

export default Checkout;

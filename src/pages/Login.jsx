import React, { useRef, useContext, useEffect } from 'react';
import {Link} from 'react-router-dom';
import '@styles/Login.scss';
import logo from '@logos/logo_yard_sale.svg';
import AppContext from '../context/AppContext';
import useGetSession from '../hooks/useGetSession';
import axios  from 'axios';

const API = 'http://localhost:3000/login'
let key=0;

const Login = () => {
	const form = useRef(null);
	const { updateSession } = useContext(AppContext);

	const handleSubmit = async (event) => {
		event.preventDefault();
		const formData = new FormData(form.current);
		
		const data = {
			name: formData.get('email'),
			password: formData.get('password')
		}

		if(data.name && data.password){
			console.log(data);
			
			const response = await axios.post(API, data);
			console.log(response);
			if(response.data.isLogin){
				updateSession(response.data);
				window.location.href = "/";
			}else{
				alert('Usuario o contraseña no validos')
			}

		}
	}

	return (
		<div className="Login">
			<div className="Login-container">
				<img src={logo} alt="logo" className="logo" />
				<form action="/" className="form" ref={form}>
					<label htmlFor="email" className="label">Email </label>
					<input type="email" name="email" placeholder="user@correo.com" className="input input-email" />
					<label htmlFor="password" className="label">Contraseña</label>
					<input type="password" name="password" placeholder="*********" className="input input-password" />
					<button className="primary-button login-button"
						onClick={handleSubmit}>
						Iniciar Sesión
					</button>
				</form>
				<Link to="/create">
				<button className="secondary-button signup-button">
					Crear Cuenta
				</button>
				</Link>
			</div>
		</div>
	);
}

export default Login;

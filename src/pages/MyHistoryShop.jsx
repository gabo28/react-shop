import React, { useContext, useState } from 'react';
import '@styles/productadmin.scss';
import service from '@hooks/useService';
import AppContext from '../context/AppContext';

const MyHistoryShop = () => {

    const { state ,updateCurrentProduct} = useContext(AppContext)
        
        const API = 'http://localhost:3000/historyshop';
        const data = service.UseGetProducts(API);
        let result = data.data ? data.data : [];
        
        console.log(data.data)

    return (
        <div className='Productlist'>
                <table>
                    <thead>
                        <tr>
                            <th>Id Compra</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Telefono</th>
                            <th>Dirección</th>
                            <th>Fecha compra</th>
                            <th>valor compra</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    {result.map((item =>
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td>{item.telefono}</td>
                                <td>{item.address}</td>
                                <td>{item.date}</td>
                                <td>{item.total}</td>
                            </tr>
                    ))}
                        
                    </tbody>
                </table>
            </div>
    );
}

export default MyHistoryShop;

import React,  { useRef, useContext, useEffect } from 'react';
import '@styles/CreateAccount.scss';
import AppContext from '../context/AppContext';

import axios  from 'axios';

const API = 'http://localhost:3000/registro'



const CreateAccount = () => {
	const form = useRef(null);
	const { updateSession } = useContext(AppContext);

	const handleSubmit = async (event) => {
		event.preventDefault();
		const formData = new FormData(form.current);
		
		const data = {
			name: formData.get('name'),
			email: formData.get('email'),
			password: formData.get('password'),
			telefono: formData.get('mobile'),
			address: formData.get('address')
		}

		if((data.name && data.password ) && data.email ){
			console.log(data);
			
			const response = await axios.post(API, data);
			console.log(response);
			if(!response.data.error){
				console.log(response.data.infoSession);
				updateSession({
					isLogin: true,
					infoSession:response.data.infoSession
				});
				window.location.href = "/";
			}else{
				alert('No se pudo completar el registro, intentelo más tarde')
			}

		}else{
			alert('Completa los datos que son requeridos')
		}
	}

	return (
		<div className="CreateAccount">
			<div className="CreateAccount-container">
				<h1 className="title">Crear Cuenta</h1>
				<form action="/" className="form" ref={form}>
					<div>
						<label for="name" className="label">Nombre*</label>
						<input type="text" name="name" placeholder="Teff" className="input input-name" />
						<label for="email" className="label">Correo*</label>
						<input type="text" name="email" placeholder="crarcuenta@example.com" className="input input-email" />
						<label for="name" className="label">Télefono</label>
						<input type="text" name="mobile" placeholder="Teff" className="input input-name" />
						<label for="name" className="label">Dirección</label>
						<input type="text" name="address" placeholder="Teff" className="input input-name" />
						<label for="password" className="label">Clave*</label>
						<input type="password" name="password" placeholder="*********" className="input input-password" />
					</div>
					<button className="primary-button login-button" onClick={handleSubmit}>Crear cuenta</button>
				</form>
			</div>
		</div>
	);
}

export default CreateAccount;

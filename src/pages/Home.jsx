import React, { useContext, useState } from 'react';
import { useParams } from 'react-router-dom';
import ProductList from '@containers/ProductList';

const Home = () => {
	const { category } = useParams('category');
	return (
		<>
			<ProductList category={category} />
		</>
	);
}

export default Home;

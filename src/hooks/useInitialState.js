import { useState } from "react";

const initialState = {
	cart: [],
	session:{},
	menu: [{id:1,title: 'Licores'}, {id:2,title: 'Snacks'}, {id:3,title: 'Bebidas'}, {id:4,title: 'Cigarreria'}],
    openDetail: false,
    itemDetail: {},
    currentProduct:{}
}

const useInitialState = () => {
	const [state, setState] = useState(initialState);

	const addToCart = (payload) => {
        if(state.cart.indexOf(payload) == -1) {
            setState({
                ...state,
                cart: [...state.cart, payload]
            });
        }else{
           state.cart[state.cart.indexOf(payload)].amount++
            setState({
                ...state
            });
        }
	};

    const removeFromCart = (payload) =>{
        state.cart[state.cart.indexOf(payload)].amount = 1
        setState({
            ...state,
            cart: state.cart.filter(items => items.id != payload.id)
        });
    }

	const updateSession = (payload) =>{
		localStorage.setItem('session', JSON.stringify(payload));
        setState({
            ...state,
            session: payload
        });
    }

	const updateSessionState = (payload) =>{
        setState({
            ...state,
            session: payload ? payload : {}
        });
    }

	const removeSessionState = () =>{
		localStorage.clear();
        setState({
            ...state,
            session: {}
        });
    }

    const toggleItemDetail = (toggle, payload = {}) =>{
        setState({
            ...state,
            openDetail: toggle,
            itemDetail: payload
        });
    }

    const updateCurrentProduct = ( payload = {}) =>{
        setState({
            ...state,
            currentProduct: payload
        });
    }

    const updateCurrentUsers = ( payload = {}) =>{
        setState({
            ...state,
            currentUsers: payload
        });
    }

	return {
		state,
		addToCart,
        removeFromCart,
		updateSession,
		updateSessionState,
		removeSessionState,
        toggleItemDetail,
        updateCurrentProduct,
        updateCurrentUsers
	}
}

export default useInitialState;
import { useEffect, useState} from "react";

const UseGetSession = () => {

    const [session, setSetssion] = useState({});

	useEffect(async ()=>{
		const infoSession = JSON.parse(localStorage.getItem('session'));
		setSetssion(infoSession);
	}, []);
	
    return session;
};

export default UseGetSession
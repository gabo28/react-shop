import { useEffect, useState} from "react";
import axios from "axios";


const UseGetProducts = (API, category=1) => {
    const [products, setProducts] = useState([]);

	useEffect(async ()=>{
		const response = await axios(API);
		setProducts(response.data);
	}, [category]);
	
    return products;
};

 export default UseGetProducts
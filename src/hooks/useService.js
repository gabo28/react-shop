import { useEffect, useState } from "react";
import axios from "axios";

const service = {};

service.UseGetProducts = (API, category = 1) => {
    const [products, setProducts] = useState([]);

    useEffect(async () => {
        const response = await axios(API);
        setProducts(response.data);
    }, [category]);

    return products;
};


service.UseGetUsers = (API, Users = 1) => {
    const [Uusers, setUsers] = useState([]);

    useEffect(async () => {
        const response = await axios(API);
        setUsers(response.data);
    }, [Users]);

    return Uusers;
};



export default service;
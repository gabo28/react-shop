import React, {useContext} from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Layout from '@containers/Layout';
import Login from '@pages/Login';
import PasswordRecovery from '@pages/PasswordRecovery';
import Home from '@pages/Home';
import Orders from '@pages/Orders';
import Checkout from '@pages/Checkout';
import NotFound from '@pages/NotFound';
import AppContext from '../context/AppContext';
import useInitialState from '../hooks/useInitialState';
import CreateAccount from '@pages/CreateAccount';
//import Historyshop from '@pages/Historyshop';
import '@styles/global.css';
import ProductsAdmin from '@pages/ProductsAdmin';
import UusersAdmin from '@pages/UusersAdmin';
import MyHistoryShop from '@pages/MyHistoryShop';
import PrivatePage from '../components/PrivatePage';






const App = () => {
	const initialState = useInitialState() || null;
	return (
		<AppContext.Provider value={initialState}>
			<BrowserRouter>
				<Layout>
					<Routes>
						<Route exact path="/" element={<Home />} />
						<Route exact path="/:category" element={<Home />} />
						<Route exact path="/login" element={<Login />} />
						<Route exact path="/recovery-password" element={<PasswordRecovery />} />
						<Route exact path="/orders" element={<Orders />} />
						<Route exact path="/checkout" element={<Checkout />} />
						<Route exact path="/MyHistoryShop" element={<MyHistoryShop />} />
						<Route exact path="/create" element={<CreateAccount />} />
						<Route exact path="*" element={<NotFound />} />
						<Route exact path="/ProductsAdmin" element={<PrivatePage><ProductsAdmin/></PrivatePage> } />
						<Route exact path="/UsersAdmin" element={<PrivatePage><UusersAdmin /></PrivatePage>} />
					
					</Routes>
				</Layout>
			</BrowserRouter>
		</AppContext.Provider>
	);
}

export default App;
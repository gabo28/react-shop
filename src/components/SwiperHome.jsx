import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Pagination } from 'swiper';
import "swiper/css";
import "swiper/css/pagination"
import "@styles/SwiperHome.scss"

const SwiperHome = ({ images }) => {
    SwiperCore.use([Pagination]);
    return (
        <>
            <Swiper pagination={true} className="SwiperHome">
                {
                    images.map((image, index) => (
                        <SwiperSlide key={`image-${index}`}><img src={image} alt="" ></img></SwiperSlide>
                    ))
                }
            </Swiper>
        </>
    );
};

export default SwiperHome;


import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import '@styles/Header.scss';
import Menu from '@components/Menu';
import menu from '@icons/icon_menu.svg';
import logo from '@logos/logo.png';
import shoppingCart from '@icons/icon_shopping_cart.svg';
import AppContext from '../context/AppContext';
import MyOrder from '@containers/MyOrder';


const Header = () => {
    const [toggle, setToggle] = useState(false);
    const [toggleOrders, setToggleOrders] = useState(false);
    const { state } = useContext(AppContext);

    const handleToggle = () => {
        setToggle(!toggle);
    }

    const handleToggleOrder = () => {
        setToggleOrders(!toggleOrders);
    }

    return (
        <nav>
            <img src={menu} alt="menu" className="menu"></img>
            <div className="navbar-left">
                <img src={logo} alt="logo" className="nav-logo"></img>
                <ul>
                    <li>
                        
                        <Link to='/'>Todos</Link>
                    </li>
                    {state.menu.map(item => (
                        <li key={item.title}>
                            <Link to={`/${item.id}`}>{item.title}</Link>
                        </li>
                    ))}
                    {state.session?.infoSession?.role == "ADMIN" && <li><Link to='/ProductsAdmin'>productos</Link></li> }
                    {state.session?.infoSession?.role == "ADMIN" && <li><Link to='/UsersAdmin'>Usuarios</Link></li>  }
                    {state.session?.infoSession?.role == "ADMIN" && <li><Link to='/MyHistoryShop'>historia de compras</Link></li>  }
                   
                </ul>
            </div>
            <div className="navbar-right">
                <ul>
                    {state.session.isLogin ?
                        <li className="navbar-email" onClick={handleToggle}>
                            {state.session.infoSession.name}
                        </li>
                        :
                        <li className="navbar-email">
                            <Link to="/login">Iniciar sesión</Link>
                        </li>
                    }

                    <li className="navbar-shopping-cart" onClick={handleToggleOrder}>
                        <img src={shoppingCart} alt="shopping cart"></img>
                        {state.cart.length > 0 ? <div>{state.cart.length}</div> : null}
                    </li>
                   
                </ul>
            </div>
            {toggle && <Menu />}
            {toggleOrders && <MyOrder />}

        </nav>
    );
};

export default Header;
import React, { useContext } from 'react';
import '@styles/ProductInfo.scss';
import imageAddToCart  from '@icons/bt_add_to_cart.svg';
import SwiperHome from '@components/SwiperHome';
import AppContext from '../context/AppContext';
import  UseGetSession from '../hooks/useGetSession';
import { Navigate } from 'react-router-dom';
const PrivatePage = ({children}) => {
	const {state} = useContext(AppContext)
	const session = JSON.parse(localStorage.getItem('session')) || null;
	console.log();
	const isLogin = session?.infoSession?.role == 'ADMIN'
	return (
		<div>
		{ isLogin ? children : <Navigate replace to="/" />}
		</div>
	);
}

export default PrivatePage;

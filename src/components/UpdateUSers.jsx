import React, { useRef, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import '@styles/Login.scss';

import AppContext from '../context/AppContext';
import '@styles/UpdateProduct.scss';
import axios from 'axios';
const API = 'http://localhost:3000/users'


const UpdateUsers = () => {

    const form = useRef(null);
    const { state, updateCurrentProduct } = useContext(AppContext)
    console.log(state.currentProduct);

    const handleSubmit = async (event) => {

        event.preventDefault();
        const formData = new FormData(form.current);
        const data = {
            id: (state.currentProduct.id),
            name: formData.get('name'),
            email: formData.get('email'),
            password: formData.get('password'),
            telefono: formData.get('telefono'),
            address: formData.get('address'),
            role: formData.get('role')
        }
        const response = await axios.put(API, data);

        console.log(response);

    }

    const crear = async (event) => {

		event.preventDefault();
		const formData = new FormData(form.current);
		const data = {
			id: (state.currentProduct.id),
            name: formData.get('name'),
            email: formData.get('email'),
            password: formData.get('password'),
            telefono: formData.get('telefono'),
            address: formData.get('address'),
            role: formData.get('role')
		}
		const response = await axios.post(API, data);
		
		console.log(response);
		
	}
    return (
        <div className='products'>
            <h3>editar   {state.currentProduct.name}</h3>
            <br />
            <form action="/" className='updateProduct' ref={form}>
                <div>
                    <input type="text" name="name" placeholder="Nombre" className="input input-name" />
                    <input type="text" name="email" placeholder="Email" className="input input-email" />
                    <input type="text" name="password" placeholder="Contraseña" className="input input-name" />
                    <br />
                    <input type="text" name="telefono" placeholder="Telefono" className="input input-name" />
                    <input type="text" name="address" placeholder="Dirección" className="input input-password" />
                    <input type="text" name="role" placeholder="Rol" className="input input-password" />
                </div>
                <button className="primary-button " onClick={handleSubmit}>Actualizar</button>
                <button className="primary-button " onClick={crear}>Crear</button>
                
                
            </form>

        </div>
    );




}
export default UpdateUsers;
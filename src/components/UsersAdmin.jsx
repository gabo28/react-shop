
import React, { useContext, useState } from 'react';
import '@styles/productadmin.scss';
import trash from '@icons/trash-can-solid.svg'
import service from '@hooks/useService';
import AppContext from '../context/AppContext';
import UpdateUsers from './UpdateUSers';
const UsersAdmin = () => {
    const API = 'http://localhost:3000/users';
    const productos = service.UseGetUsers(API);
    const { state, updateCurrentProduct } = useContext(AppContext)
    let data = {}
    const setDataProduct = (item) => {
        updateCurrentProduct(item)

    }


    return (
        <>

            <div className='Productlist'>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Contraseña</th>
                            <th>Telefono</th>
                            <th>Dirección</th>
                            <th>Rol</th>
                            <th>dirección</th>
                            <th>editar</th>
                            

                        </tr>
                    </thead>
                    <tbody>
                        {productos.map(item => (
                            <tr key={item.id}>
                                <td >{item.id}</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td>{item.password}</td>
                                <td>{item.telefono}</td>   
                                <td>{item.address}</td>
                                <td>{item.role}</td>
                                <td>{item.address}</td>
                                <td> <button onClick={() => setDataProduct(item)}>editar </button> </td>
                                
                                
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <UpdateUsers/>
            
        </>
    )

}

export default UsersAdmin;
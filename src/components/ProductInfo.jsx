import React, { useContext } from 'react';
import '@styles/ProductInfo.scss';
import imageAddToCart  from '@icons/bt_add_to_cart.svg';
import SwiperHome from '@components/SwiperHome';
import AppContext from '../context/AppContext';

const ProductInfo = () => {
	const {state, addToCart} = useContext(AppContext)
	
	const handleAddToCart = product =>{
		console.log(product);
		addToCart(product);
	}

	return (
		<>
			<SwiperHome images={state.itemDetail.images}/>
			<div className="ProductInfo">
				<p>${state.itemDetail.price}</p>
				<p>{state.itemDetail.title}</p>
				<p>{state.itemDetail.description}</p>
				<button className="primary-button add-to-cart-button" onClick={() => handleAddToCart(state.itemDetail)}>
					<img src={imageAddToCart} alt='Imagen agregar al carrito'></img>
					Agregar
				</button>
			</div>
		</>
	);
}

export default ProductInfo;

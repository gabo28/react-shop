import React, { useRef, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import '@styles/Login.scss';
import AppContext from '../context/AppContext';
import '@styles/UpdateProduct.scss';
import axios from 'axios';
const API = 'http://localhost:3000/products'


const UpdateProduct = () => {
	const form = useRef(null);
	const { state, updateCurrentProduct } = useContext(AppContext)
	console.log(state.currentProduct);
	
	const handleSubmit = async (event) => {

		event.preventDefault();
		const formData = new FormData(form.current);
		const data = {
			id: (state.currentProduct.id),
			title: formData.get('title'),
			price: formData.get('price'),
			stock: formData.get('stock'),
			url_image: formData.get('url_imagen'),
			description: formData.get('description'),
			category_id: formData.get('Category')
		}
		const response = await axios.put(API, data);
		
		console.log(response);
		
		alert('se actualizado correctamente el producto porfavor vuelva a cargar la pagina ')
	}


	const crear = async (event) => {

		event.preventDefault();
		const formData = new FormData(form.current);
		const data = {
			id: (state.currentProduct.id),
			title: formData.get('title'),
			price: formData.get('price'),
			stock: formData.get('stock'),
			url_image: formData.get('url_imagen'),
			description: formData.get('description'),
			category_id: formData.get('Category')
		}
		const response = await axios.post(API, data);
		
		console.log(response);
		alert('se creado correctamente el producto porfavor vuelva a cargar la pagina ')
		
	}

	

	return (
		<div className='products'>
			<h3>editar   {state.currentProduct.title}</h3>
			<br />
			<form action="/" className='updateProduct' ref={form}>
				<div>
					<input type="text" name="title" placeholder="Titulo" className="input input-name" />
					<input type="number" name="price" placeholder="Precio" className="input input-email" />
					<input type="number" name="stock" placeholder="stock" className="input input-name" />
					<br />
					<input type="url" name="url_imagen" placeholder="imagen" className="input input-name" />
					<input type="text" name="description" placeholder="descripción" className="input input-password" />
					<input type="number" name="Category" placeholder="categoria" className="input input-password"  />

					
				</div>
				<button className="primary-button " onClick={handleSubmit}>Actualizar</button>
				<button className="primary-button " onClick={crear}>Crear</button>
				
			</form>
			
		</div>
	);

}

export default UpdateProduct;
import React, {useContext, useEffect} from 'react';
import '@styles/Menu.scss';
import AppContext from '../context/AppContext';
const Menu = () => {
	const { removeSessionState } = useContext(AppContext);
	
	
	const handleRemoveSession = () => {
		removeSessionState()
		window.location.href = "/";
		}

	return (

		<div className="Menu">
			<ul>
				
				<li>
					<button className="closeSession" onClick={handleRemoveSession}>Cerrar session </button>
				</li>
			</ul>
		</div>
	);
}

export default Menu;

import React, { useContext, useState } from 'react';
import '@styles/productadmin.scss';
import trash from '@icons/trash-can-solid.svg'
import service from '@hooks/useService';
import UpdateProduct from './UpdateProduct';
import AppContext from '../context/AppContext';

const ProductsAdmin = () => {
    const API = 'http://localhost:3000/products';
    const productos = service.UseGetProducts(API);
    const {state, updateCurrentProduct} = useContext(AppContext)
    let data={}
    const setDataProduct = (item) =>{
     updateCurrentProduct(item)

    }
    console.log(productos);
    return (
        <>
           
            <div className='Productlist'>
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Titulo</th>
                            <th>Precio</th>
                            <th>stock</th>
                            <th>imagen</th>
                            <th>descripción</th>
                            <th>categoria</th>
                            <th>editar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {productos.map(item => (
                            <tr key={item.id}>
                                <td >{item.id}</td>
                                <td>{item.title}</td>
                                <td>{item.price}</td>
                                <td>{item.stock}</td>
                                <td>{item.images}</td>
                                <td>{item.description}</td>
                                <td>{item.category}</td>
                                <td> <button onClick={() =>setDataProduct(item)}>editar</button> </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <UpdateProduct/>
        </>
    )


}


export default ProductsAdmin;